package model.vo;

import model.data_structures.MiLista;

public class Company implements Comparable <Company>
	{
	
	private String nombre;
	private MiLista<Servicio> taxis;
	
	public Company(String pNombre, MiLista<Servicio> pTaxis)
	{
		nombre = pNombre;
		taxis = pTaxis;
	}
	
	public String getName()
	{
		return nombre;
	}
	
	public MiLista<Servicio> getTaxiList()
	{
		return taxis;
	}

	@Override
	public int compareTo(Company o) 
	{
		if (this.taxis.darTamanio() > o.taxis.darTamanio())
		{
			return 1;
		}
		
		else if (this.taxis.darTamanio() < o.taxis.darTamanio())
		{
			return -1;
		}
		
		return 0;
	}
	

}
