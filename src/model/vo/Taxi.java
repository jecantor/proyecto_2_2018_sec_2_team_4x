package model.vo;

import model.data_structures.MiLista;
import model.data_structures.SCHashTable;

public class Taxi implements Comparable<Taxi> {

	private SCHashTable<Integer,MiLista<Servicio>> sEnZona;
	private String taxi_id;
	private String company;
	
	private MiLista <Servicio> services = new MiLista <Servicio>();
	
	public Taxi(String pTaxi_id, String pCompany)
	{
		taxi_id = pTaxi_id;
		company = pCompany;
	}
	
	public void setServices (MiLista <Servicio> p)
	{
		services = p;
	}
	
	public MiLista<Servicio> getServiceList()
	{
		return services;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() 
	{
		return company;
	}
	
	
	@Override
	public int compareTo(Taxi o)
	{
		if (this.TaxiPoints() > o.TaxiPoints()) return 1;
		else if (this.TaxiPoints() < o.TaxiPoints()) return -1;
		return 0;
	}
	
	public double TaxiPoints()
	{
		double totalMoney = 0;
		double totalMiles = 0;
		
		if (services.darTamanio() == 1)
		{
			totalMoney = services.darPrimerElemento().getTripTotal();
			totalMiles = services.darPrimerElemento().getTripMiles();
		}
		
		else
		{
		
		services.iniciarRecorrido();
		
		while (services.hasNext())
		{
			totalMoney += services.darElementoActual().getTripTotal();
			totalMiles += services.darElementoActual().getTripMiles();
			services.avanzar();
		}
		}
		
		
		return (totalMoney/totalMiles)*services.darTamanio();
	}
}
