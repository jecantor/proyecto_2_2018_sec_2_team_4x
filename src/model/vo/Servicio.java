package model.vo;

public class Servicio implements Comparable<Servicio>{
	
	private String trip_id;
	private String taxi_id;
	private String trip_seconds;
	private String trip_miles;
	private String pickup_community_area;
	private String dropoff_community_area;
	private String trip_start_timestamp;
	private String trip_end_timestamp;
	private String company;
	private String trip_total;
	private String pickup_centroid_latitude;
	private String pickup_centroid_longitude;
	
	public Servicio(String pCompany,String taxiId,String pPickup_centroid_latitude, String pPickup_centroid_longitude,String pTrip_id, String pTrip_seconds, String pTrip_miles, String pPickup_community_area,
					String pTrip_start_timestamp, String pTrip_end_timestamp, String pTripTotal, String pDropoff_community_area)
	{
		company = pCompany;
		taxi_id = taxiId;
		trip_id = pTrip_id;
		trip_seconds = pTrip_seconds;
		trip_miles = pTrip_miles;
		pickup_community_area = pPickup_community_area;
		trip_start_timestamp = pTrip_start_timestamp;
		trip_end_timestamp = pTrip_end_timestamp;
		trip_total = pTripTotal;
		pickup_centroid_latitude = pPickup_centroid_latitude;
		pickup_centroid_longitude = pPickup_centroid_longitude;
		dropoff_community_area = pDropoff_community_area;
	}
	
	public double getLatitude()
	{
		return Double.parseDouble(pickup_centroid_latitude);
		
		
	}
	
	public double getLongitude()
	{
		return Double.parseDouble(pickup_centroid_longitude);
	}
	
	
	public String getCompany()
	{
		return company;
	}
	
	public String getCommunityArea()
	{
		return pickup_community_area;
	}
	
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return trip_id;
	}	


	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return Double.parseDouble(trip_miles);
	}

	
	public String getTripStartTimeStamp()
	{
		return trip_start_timestamp;
	}
	
	public String getTripEndTimeStamp()
	{
		return trip_end_timestamp;
	}
	
	public double getTripTotal()
	{
		return Double.parseDouble(trip_total);
	}
	
	public String getFechaI()
	{
		if (trip_start_timestamp.equals("NaN")) return "No Registra";
		String startS[] = trip_start_timestamp.split("T");
		String f = startS[0];
		String rta[] = f.split("-");
		return rta[0] + rta[1] + rta[2];
	}
	
	public String getFechaF()
	{
		if (trip_end_timestamp.equals("NaN")) return "No Registra";
		String startS[] = trip_end_timestamp.split("T");
		String f = startS[0];
		String rta[] = f.split("-");
		return rta[0] + rta[1] + rta[2];
	}
	
	public String getHoraI()
	{
		if (trip_start_timestamp.equals("NaN")) return "No Registra";
		String startS[] = trip_start_timestamp.split("T");
		String t = startS[1];
		String a[] = t.split(":");
		return a[0] + a[1];
	}
	
	public String getHoraF()
	{
		if (trip_end_timestamp.equals("NaN")) return "No Registra";
		String startS[] = trip_end_timestamp.split("T");
		String t = startS[1];
		String a[] = t.split(":");
		return a[0] + a[1];
	}
	
	public int getKey2A()
	{
		int seconds = Integer.parseInt(trip_seconds);
		
		while (true)
		{
			int i = 1;
			int f = 60;
			
			if (seconds >= i && seconds <= f)
			{
				return f;
			}
			
			i += 60;
			f += 60;
		}
		
		
	}


        /**
         * @return id - Taxi_id
         */
        public String getTaxiId() {
            // TODO Auto-generated method stub
            return taxi_id;
        }

        /**
         * @return time - Time of the trip in seconds.
         */
        public int getTripSeconds() {
            // TODO Auto-generated method stub
            return Integer.parseInt(trip_seconds);
        }


        public String getStartTime(){
            //TODO Auto-generated method stub
            return trip_start_timestamp;
        }

        public int getPickupZone(){
            return Integer.parseInt(pickup_community_area);
        }

        public int getDropOffZone(){
            return Integer.parseInt(dropoff_community_area);
        }
        
        public String getStringDropOffZone()
        {
        	return dropoff_community_area;
        }
        
        public String getStringPickupZone()
        {
        	return dropoff_community_area;
        }


        public double getPickupLatitud(){
            //TODO Auto-generated method stub
            return -1;
        }

        public double getPickupLongitud(){
            //TODO Auto-generated method stub
            return -1;
        }
        
        public int darHorasI()
        {
        	String h[] = trip_start_timestamp.split("T");
        	String h1[] =  h[1].split(":");
        	int rta = Integer.parseInt(h1[0]);
        	return rta;
        }
        	
        public int darMinutosI()
        {
        	String h[] = trip_start_timestamp.split("T");
        	String h1[] =  h[1].split(":");
        	int rta = Integer.parseInt(h1[1]);
        	return rta;
        }

        public String darFechaI()
        {
        	String h[] = trip_start_timestamp.split("T");
        	return h[0];
        }
        
		@Override
		public int compareTo(Servicio o) {
			
			int f1 = Integer.parseInt(getFechaI());
			int f2 = Integer.parseInt(o.getFechaI());
			
			int h1 = Integer.parseInt(getHoraI());
			int h2 = Integer.parseInt(o.getHoraI());
			
			int rta;
			
			if(f1 == f2)
			{
				if(h1 == h2)
				{
					rta = 0;
				}
				else if(h1 < h2)
				{
					rta = -1;
				}
				else
				{
					rta = 1;
				}
			}
			else if(f1 < f2)
			{
				rta = -1;
			}
			else
			{
				rta = 1;
			}
			
			return rta;
			
		}

}