package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.MiLista;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

    private String taxiId;
    private String compania;
    private LinkedList<Servicio> servicios;

    public TaxiConServicios(String taxiId, String compania){
        this.taxiId = taxiId;
        this.compania = compania;
        servicios = new MiLista <Servicio>();
        // this.servicios = new List<Service>(); // inicializar la lista de servicios 
    }

    public String getTaxiId() {
        return taxiId;
    }

    public String getCompania() {
        return compania;
    }

    public LinkedList<Servicio> getServicios()
    {
    	return servicios;
    }
    
    public int numeroServicios(){
        return servicios.darTamanio();
    }

    public void agregarServicio(Servicio servicio){
        servicios.agregarElemento(servicio);
    }

    @Override
    public int compareTo(TaxiConServicios o) {
        return taxiId.compareTo( o.getTaxiId());
    }

    public void print(){
        System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
        for(int i = 0; i < servicios.darTamanio(); i ++){
        	Servicio s = servicios.darElementoPosicion(i);
            System.out.println("\t"+s.getStartTime());
        }
        System.out.println("\n___________________________________\n");;
    }
}
