package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;
import com.google.gson.JsonSyntaxException;

import API.ITaxiTripsManager;
import model.data_structures.HeapSort;
import model.data_structures.LinkedList;
import model.data_structures.MergeSort;
import model.data_structures.MiLista;
import model.data_structures.RedBlackBST;
import model.data_structures.SCHashTable;
import model.vo.Company;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class TaxiTripsManager implements ITaxiTripsManager {
	
	//Cantor

	private MiLista <Servicio> inicial = new MiLista <Servicio>();

	private RedBlackBST <Company,String> companyTree = new RedBlackBST();

	private SCHashTable<String,MiLista<Servicio>> HTAreaServices = new SCHashTable<String,MiLista<Servicio>>();

	private SCHashTable<Integer,MiLista<Servicio>> HTSeconds = new SCHashTable<Integer,MiLista<Servicio>>();

	private SCHashTable <String, MiLista<Servicio>> loadingHT = new SCHashTable <String, MiLista <Servicio>>();

	private SCHashTable <String, MiLista <Servicio>> taxis = new SCHashTable <String, MiLista <Servicio>>();

	private SCHashTable <String, RedBlackBST <String,Servicio>> milesServices = new SCHashTable <String, RedBlackBST <String,Servicio>>();
	
	private DecimalFormat format = new DecimalFormat("0.0");
	
	private DecimalFormat format2 = new DecimalFormat("0.00");

	//Sebastian
	private SCHashTable<Double, MiLista<Servicio>> table1B = new SCHashTable<Double, MiLista<Servicio>>();
	
	private SCHashTable<String,RedBlackBST<String,Servicio>> table2B = new SCHashTable<String,RedBlackBST<String,Servicio>>();
	
	private RedBlackBST<String,Servicio> req3C = new RedBlackBST<String,Servicio>();

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	@Override
	public boolean cargarSistema(String direccionJson)
	{
		System.out.println("Inside loadServices with File:" + direccionJson);

		try {

			JsonStreamParser j = new JsonStreamParser((new FileReader(direccionJson)));

			while (j.hasNext())
			{
				JsonArray array = j.next().getAsJsonArray();

				for (int i = 0; array != null && i < array.size(); i++)
				{

					JsonObject obj= (JsonObject) array.get(i);

					String company = "Independent Owner";
					if( obj.get("company") != null)
					{ company = obj.get("company").getAsString(); }

					String pickup_centroid_latitude = "NaN";
					if( obj.get("pickup_centroid_latitude") != null)
					{ pickup_centroid_latitude = obj.get("pickup_centroid_latitude").getAsString(); }

					String pickup_centroid_longitude = "NaN";
					if( obj.get("pickup_centroid_longitude") != null)
					{ pickup_centroid_longitude = obj.get("pickup_centroid_longitude").getAsString(); }


					String taxi_id = "NaN";
					if( obj.get("taxi_id") != null)
					{ taxi_id = obj.get("taxi_id").getAsString();}

					String pickup_community_area = "NaN";
					if( obj.get("pickup_community_area") != null)
					{ pickup_community_area = obj.get("pickup_community_area").getAsString(); }
					
					String dropoff_community_area = "NaN";
					if( obj.get("dropoff_community_area") != null)
					{ dropoff_community_area = obj.get("dropoff_community_area").getAsString(); }
					
					String trip_end_timestamp = "NaN";
					if( obj.get("trip_end_timestamp") != null)
					{ trip_end_timestamp = obj.get("trip_end_timestamp").getAsString(); }

					String trip_id = "NaN";
					if( obj.get("trip_id") != null)
					{ trip_id = obj.get("trip_id").getAsString(); }

					String trip_total = "0.0";
					if( obj.get("trip_total") != null)
					{ trip_total = obj.get("trip_total").getAsString(); }

					String trip_miles = "0.0";
					if( obj.get("trip_miles") != null)
					{ trip_miles = obj.get("trip_miles").getAsString(); }

					String trip_seconds = "0";
					if( obj.get("trip_seconds") != null)
					{ trip_seconds = obj.get("trip_seconds").getAsString(); }

					String trip_start_timestamp = "NaN";
					if( obj.get("trip_start_timestamp") != null)
					{ trip_start_timestamp = obj.get("trip_start_timestamp").getAsString(); }
					
					if (trip_total.compareTo("0.0") > 0 && trip_miles.compareTo("0.0") > 0)
					{
					Servicio s = new Servicio(company,taxi_id,pickup_centroid_latitude,pickup_centroid_longitude,trip_id, trip_seconds, trip_miles, pickup_community_area, trip_start_timestamp, trip_end_timestamp,trip_total, dropoff_community_area);
					
					
					
					
					if (taxis.get(s.getTaxiId()) != null)
					{
						
						taxis.get(taxi_id).agregarElemento(s);
					}
					else
					{
						if(!s.getTaxiId().equals("NaN"))
						{
							
							MiLista <Servicio> add = new MiLista <Servicio> ();
							add.agregarElemento(s);
							taxis.put(taxi_id, add);
							
						}
					}
					
					

					if (HTAreaServices.get(pickup_community_area)!= null)
					{
						HTAreaServices.get(pickup_community_area).agregarElemento(s);
					}

					else
					{
						MiLista <Servicio> add = new MiLista <Servicio> ();
						add.agregarElemento(s);
						HTAreaServices.put(pickup_community_area, add);
					}

					if (loadingHT.get(company) != null)
					{
						loadingHT.get(company).agregarElemento(s);
					}

					else
					{
						MiLista <Servicio> add = new MiLista <Servicio> ();
						add.agregarElemento(s);
						loadingHT.put(company, add);
					}



					if (HTSeconds.get(secondsToKey(Integer.parseInt(trip_seconds)))!= null)
					{
						HTSeconds.get(secondsToKey(Integer.parseInt(trip_seconds))).agregarElemento(s);
					}

					else
					{
						MiLista <Servicio> add = new MiLista <Servicio> ();
						add.agregarElemento(s);
						HTSeconds.put(secondsToKey(Integer.parseInt(trip_seconds)), add);
					}

					//Carga 1B -----------------------------------------------------------------------------
					
					double miles = Double.parseDouble(trip_miles);
					
					if(trip_miles != "NaN")
					{
						if(table1B.get(miles) == null)
						{
							MiLista<Servicio> sL = new MiLista<Servicio>();
							sL.agregarElemento(s);
							table1B.put(miles, sL);
						}
						else
						{
							MiLista<Servicio> sL = table1B.get(miles);
							sL.agregarElemento(s);
							table1B.put(miles, sL);
						}
					}
					
					//Carga 2B---------------------------------------------------------------------
					
					if(pickup_community_area != "NaN" && dropoff_community_area != "NaN")
					{
						
						String zona = pickup_community_area + "-" + dropoff_community_area;
						
						if(table2B.get(zona) == null)
						{
							RedBlackBST<String,Servicio> arbol = new RedBlackBST<String,Servicio>();
							arbol.put(trip_start_timestamp + ";" + trip_end_timestamp, s);
							table2B.put(zona, arbol);
						}
						else
						{
							RedBlackBST<String,Servicio> arbol = table2B.get(zona);
							arbol.put(trip_start_timestamp + ";" + trip_end_timestamp, s);
							table2B.put(zona, arbol);
						}
						
					}
					
					//Carga 3C------------------------------------------------------------------------
						if(!trip_start_timestamp.equalsIgnoreCase("NaN") && !trip_end_timestamp.equalsIgnoreCase("NaN"))
						{
							if(!pickup_community_area.equals("NaN") && !dropoff_community_area.equals("NaN"))
							{
								if(!pickup_community_area.equalsIgnoreCase(dropoff_community_area))
								{
									req3C.put(trip_id, s);
								}
							}
						}
					
					//--------------------------------------------------------------------------------
					
					if (!pickup_centroid_latitude.equals("NaN") && !pickup_centroid_longitude.equals("NaN")) inicial.agregarElemento(s);
					//contador ++;
					//System.out.println(company + "\n");

				}

			}

			MiLista <MiLista<Servicio>> a = loadingHT.values();

			a.iniciarRecorrido();

			while (a.hasNext())
			{
				String s = a.darElementoActual().darPrimerElemento().getCompany();
				Company add = new Company (s, a.darElementoActual());
				companyTree.put(add,s);
				a.avanzar();

			}
			
			
			
			return true;
			

		}
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();
		}
		catch (Exception e4)
		{
			e4.printStackTrace();
		}

		return false;
	}


	@Override
	public LinkedList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania)
	{
		
		
		MiLista<Servicio> add = new MiLista <Servicio>();
		
		SCHashTable <String, TaxiConServicios> temp = new SCHashTable<String, TaxiConServicios >();

		try
		{
			boolean x = false;
			while (!companyTree.isEmpty() && !x)
			{
				Company c = companyTree.max();

				if (c.getName().equals(compania))
				{
					MiLista <Servicio> l = c.getTaxiList();

					l.iniciarRecorrido();

					while (l.hasNext() && !x)
					{
						if (!l.darElementoActual().getCommunityArea().equals("NaN"))
							if (Integer.parseInt(l.darElementoActual().getCommunityArea()) == zonaInicio)
							{
								add.agregarElemento(l.darElementoActual());
								x = true;
								break;

							}

						l.avanzar();
					}
				}

				companyTree.deleteMax();	
			}


			MiLista <Servicio> list = HTAreaServices.get(Integer.toString(zonaInicio));

			if (list != null)
			{
				list.iniciarRecorrido();

				while (list.hasNext())
				{


					if (list.darElementoActual().getCompany().equals(compania))
					{
						add.agregarElemento(list.darElementoActual());

					}

					list.avanzar();
				}
			}


		}

		catch (Exception e)
		{
			e.printStackTrace();
		}

		Servicio [] n = new Servicio [add.darTamanio()-1];

		add.iniciarRecorrido();

		int e = 0;
		while (add.hasNext())
		{
			n[e] = add.darElementoActual();
			add.avanzar();
			e++;
		}

		MergeSort.sort(n);
		
		
		for (Servicio i : n)
		{
			TaxiConServicios a = new TaxiConServicios (i.getTaxiId(), i.getCompany());
			
			if (temp.get(i.getTaxiId()) != null)
			{
				temp.get(i.getTaxiId()).agregarServicio(i);
			}
			
			else
			{
				a.agregarServicio(i);
				temp.put(i.getTaxiId(), a);
			}
		}



		return temp.values();

	}


	@Override
	public LinkedList<Servicio> A2ServiciosPorDuracion(int duracion)
	{
		
		return HTSeconds.get(secondsToKey(duracion));
		//return new MiLista<Servicio>();
	}


	@Override
	public LinkedList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		
		MiLista<Servicio> rta = new MiLista<Servicio>();
		
		MiLista<Double> keys = (MiLista<Double>) table1B.keys();
		MiLista<Double> keysEnRango = new MiLista<Double>();
		
		for(int i = 0; i < keys.darTamanio(); i++)
		{
			double llave = keys.darElementoPosicion(i);
			if(llave >= distanciaMinima && llave <= distanciaMaxima)
			{
				keysEnRango.agregarElemento(llave);
			}
		}
		
		for(int j = 0; j < keysEnRango.darTamanio(); j++)
		{
			double llave = keysEnRango.darElementoPosicion(j);
			MiLista<Servicio> lt = table1B.get(llave);
			
			for(int k = 0; k < lt.darTamanio(); k++)
			{
				Servicio s = lt.darElementoPosicion(k);
				rta.agregarElemento(s);
			}
		}
		
		return rta;
	}


	@Override
	public LinkedList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		
		String zona = zonaInicio + "-" + zonaFinal;
		
		RedBlackBST<String, Servicio> arbol = table2B.get(zona);
		MiLista<String> lista = arbol.keys();
		MiLista<Servicio> rta = new MiLista<Servicio>();
		
		String[] fI1 = fechaI.split("-");
		int fI2 = Integer.parseInt(fI1[0] + fI1[1] + fI1[2]);
		
		String[] fF1 = fechaF.split("-");
		int fF2 = Integer.parseInt(fF1[0] + fF1[1] + fF1[2]);
		
		String[] hI1 = horaI.split(":");
		int hI2 = Integer.parseInt(hI1[0] + hI1[1]);
		
		String[] hF1 = horaF.split(":");
		int hF2 = Integer.parseInt(hF1[0] + hF1[1]);
		
		for(int i = 0; i < lista.darTamanio(); i++)
		{
			String[] glob = lista.darElementoPosicion(i).split(";");
			
			String[] fIK = glob[0].split("T");
			String[] fIK1 = fIK[0].split("-");
			int fIK2 = Integer.parseInt(fIK1[0] + fIK1[1] + fIK1[2]);
			
			String[] fFK = glob[1].split("T");
			String[] fFK1 = fFK[0].split("-");
			int fFK2 = Integer.parseInt(fFK1[0] + fFK1[1] + fFK1[2]);
			
			if(fIK2 >= fI2 && fFK2 <= fF2)
			{
				String[] hIK1 = fIK[1].split(":");
				int hIK2 = Integer.parseInt(hIK1[0] + hIK1[1]);
				
				String[] hFK1 = fFK[1].split(":");
				int hFK2 = Integer.parseInt(hFK1[0] + hFK1[1]);
				
				if(hIK2 >= hI2 && hFK2 <= hF2)
				{
					try {
						rta.agregarElemento(arbol.get(lista.darElementoPosicion(i)));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
			
		}
		
		return rta;
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		
		int index = 0;
		MiLista <String> l = (MiLista <String>) taxis.keys();

		TaxiConPuntos [] array = new TaxiConPuntos [l.darTamanio()-1];

		

		l.iniciarRecorrido();

		while (l.hasNext())
		{
			MiLista <Servicio> add = taxis.get(l.darElementoActual());
			array[index] = new TaxiConPuntos (add.darPrimerElemento().getTaxiId(),add.darPrimerElemento().getCompany());
			array[index].setServices(add);
			index++;
			l.avanzar();

		}

		HeapSort.sort(array);

		return array;

	}

	@Override
	public LinkedList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, String millasReq2C) {
		

		
		setFormat();
		
		MiLista <Servicio> r = new MiLista <Servicio>();
		
		
		try
		{

		String ref = calcularReferencia();
		
		double lat = Double.parseDouble(ref.split("T")[0]);
		
		double lon = Double.parseDouble(ref.split("T")[1]);
		
		inicial.iniciarRecorrido();
		
		while (inicial.hasNext())
		{
			
			double d = getDistance(lat,lon,inicial.darElementoActual().getLatitude(),inicial.darElementoActual().getLongitude());
			

			
			//System.out.println("Get Distance: " + d);
			
			String key = format.format(d);
			
			
			//System.out.println("Key: " + key);
			
			if (milesServices.get(key) != null)
			{
				milesServices.get(key).put(inicial.darElementoActual().getTaxiId(), inicial.darElementoActual());
			}
			else
			{
				RedBlackBST <String,Servicio> add = new RedBlackBST <String,Servicio>();
				add.put(inicial.darElementoActual().getTaxiId(), inicial.darElementoActual());		
				milesServices.put(key,add);
			}
			
			inicial.avanzar();
			
		}
		
		
		RedBlackBST <String,Servicio> search = milesServices.get(millasReq2C);
		if (search == null)
		{
			return r;
		}
		
		while (!search.isEmpty())
		{
			
			Servicio s = search.get(search.min());
			
			System.out.println("Taxi ID in Range: " + s.getTaxiId());
			
			if (s.getTaxiId().equals(taxiIDReq2C)) r.agregarElemento(s);
			
			
			search.deleteMin();
		}
		
		if (r.darTamanio() == 0) return r; 
		
		if (r.darTamanio() == 1)
		{
			System.out.println("___________________________________ \n ");
			System.out.println("Trip ID: " + r.darPrimerElemento().getTripId());
			System.out.println("Latitude: " + r.darPrimerElemento().getLatitude());
			System.out.println("Longitude: " + r.darPrimerElemento().getLongitude());
			System.out.println("Distancia de LR (aprox) en millas: " + format.format(getDistance(r.darPrimerElemento().getLatitude(),r.darPrimerElemento().getLongitude(),lat,lon)));
			System.out.println("___________________________________ ");
		}
		
		else
		{
			
		r.iniciarRecorrido();
		
		while (r.hasNext())
		{
			System.out.println("___________________________________ \n");
			System.out.println("Trip ID: " + r.darElementoActual().getTripId());
			System.out.println("Latitude: " + r.darElementoActual().getLatitude());
			System.out.println("Longitude: " + r.darElementoActual().getLongitude());
			System.out.println("Distancia de LR (aprox) en millas: " + format.format(getDistance(r.darElementoActual().getLatitude(),r.darElementoActual().getLongitude(),lat,lon)));
			System.out.println("___________________________________ ");
			r.avanzar();
		}
		
		}
		
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return r;
		
		}
	
	

	@Override
	public LinkedList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {

				MiLista<String> llaves = req3C.keys();
				MiLista<Servicio> lista = new MiLista<Servicio>();
				
				String h[] = hora.split(":");
				int horas = Integer.parseInt(h[0]);
				int minutos = Integer.parseInt(h[1]);
				
				
				System.out.println(minutos);
				
				for(int i = 0; i < llaves.darTamanio(); i++)
				{
					try {
						Servicio s = req3C.get(llaves.darElementoPosicion(i));
						int minutosI = s.darMinutosI();
						String fechax = s.darFechaI();
						
						if(fecha.equalsIgnoreCase(fechax))
						{
							if(s.darHorasI() == horas)
							{
								if(minutosI == 0 && minutos < 15)
								{
									lista.agregarElemento(s);
								}
								else if(minutosI == 15 && minutos >= 15 && minutos < 30)
								{
									lista.agregarElemento(s);
								}
								else if(minutosI == 30 && minutos >=30 && minutos < 45)
								{
									lista.agregarElemento(s);
								}
								else if(minutosI == 45 && minutos >= 45 && minutos < 60)
								{
									lista.agregarElemento(s);
								}
								
							}
						}
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				return lista;
	}

	int secondsToKey (int p)
	{
		int i = 60;
		while (true)
		{
			if (p <= i) return i;

			i += 60;
		}

	}

	double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
		
		final int R = 6371*1000; // Radious of the earth in meters
		Double latDistance = Math.toRadians(lat2-lat1);
		Double lonDistance = Math.toRadians(lon2-lon1);
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))
		* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance * 0.000621371;
	}
	
	String calcularReferencia()
	{
		double sumLon = 0.0;
		double sumLat = 0.0;
		
		inicial.iniciarRecorrido();
		
		while (inicial.hasNext())
		{
			
			sumLon += inicial.darElementoActual().getLongitude();
			sumLat += inicial.darElementoActual().getLatitude();
			inicial.avanzar();
		}
		
		
		return (sumLat/inicial.darTamanio())+"T"+(sumLon/inicial.darTamanio());
	}
	
	String milesToKey(double d)
	{
		
		d = Double.parseDouble(format.format(d));
		
		double i = 0.0;
		double f = 0.1;
		boolean e = false;
		
		while (!e)
		{
			//System.out.println("d: " + d);
			//System.out.println("i: " + i);
			//System.out.println("f: " + f);
			
			if (d >= i && d < 0.1)
			{
				System.out.println(format.format(f));
				e = true;
				break;
				
			}
			
			i += 0.1;
			f += 0.1;
		}
		
		return format.format(f);
	}
	
	void setFormat()
	{
		Locale currentLocale = Locale.getDefault();
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(','); 
		format = new DecimalFormat("0.0", otherSymbols);
		format2 = new DecimalFormat("0.00",otherSymbols);
	}

}

